const apiKey = 'af4759406a99785c35e5ace79fdb6165'
const token = 'b54c7a473160d3ebcbe1425585e225e1cc4a8d437346b11c5e690fd3354f2a49'
const commonUrl = 'https://api.trello.com/1/'
const boardId = '5dea1c288a9df04607066547'
const boardUrl = `${commonUrl}boards/${boardId}?fields=name&lists=all&list_fields=all&url&key=${apiKey}&token=${token}`;


getData(boardUrl).then(makeTheBoard);


// fetch the data for a given url
async function getData(url) {
    const response = await fetch(url)
    const data = await response.json();
    return data;
}


// making the board on local html
async function makeTheBoard(boardData) {
    updateTitle(boardData);
    for (let listNumber = 0; listNumber < boardData.lists.length; listNumber++) {
        await fetchCardsAndMakeList(boardData.lists[listNumber]);
    }
}

// Changing the title for board name
function updateTitle(boardData) {
    let title = document.querySelector('title');
    title.innerText = boardData.name;
}


// pasting lists on local html imported from the board
async function fetchCardsAndMakeList(list) {
    let thisListUrl = `${commonUrl}lists/${list.id}/cards?fields=all&url&key=${apiKey}&token=${token}`;
    let dataOfCardsOnThisList = await getData(thisListUrl);
    makeList(dataOfCardsOnThisList, list);
}

// making the local list
function makeList(dataOfCardsOnThisList, list) {
    let listData = createNewList(list);
    addCardsToListData(dataOfCardsOnThisList, listData);
    appendListOnBoard(listData);
}


// Creating a new list for local html
function createNewList(list) {
    let listData = document.createElement('div');
    listData.id = list.id;
    listData.innerHTML = `<h1 style = "white-space: nowrap;">${list.name}</h1>`;
    listData.innerHTML = listData.innerHTML + `<button onclick = addCardFromUser("${listData.id}") style = "margin-bottom: 5%;">Add a new Card</button>`;
    return listData;
}



// Add all the cards that belongs to this list

function addCardsToListData(dataOfCardsOnThisList, listData) {
    for (let card = 0; card < dataOfCardsOnThisList.length; card++) {
        addCard(listData, dataOfCardsOnThisList[card].name, dataOfCardsOnThisList[card].id);
    }
}


// Add the card in local html to given list id
function addCard(listData, name, cardId) {
    listData.innerHTML = listData.innerHTML + `<div style = "margin-bottom: 10%; display: flex; justify-content: streach;" id = "${cardId}"> 
                                                    <div style="white-space: nowrap;">${name}</div>
                                                    <button onclick=renameTheCard("${cardId}") style = "float: right; margin-left: 10%;max-height: 30px;">rename</button>
                                                    <button onclick=deleteTheCard("${cardId}") style = "float: right; max-height: 30px;">delete</button>
                                                    </div>`;

}



// append the new list to local html

function appendListOnBoard(listData) {
    let appendHere = document.querySelector('.listsonthisboard');
    appendHere.appendChild(listData);
}


// Add a new card to the list
async function addCardFromUser(listID) {
    let listData = document.getElementById(listID);
    let name = prompt('Name your card!');
    name = name.trim();
    if(name != null && name != '' && name != ' ')
        addCardOnboard(listData, name);
}

// delete the card
function deleteTheCard(cardId) {
    let card = document.getElementById(cardId);
    card.parentNode.removeChild(card);
    deleteOnBoard(cardId);
}


// rename the card

function renameTheCard(idForboard) {
    let x = document.getElementById(idForboard).firstElementChild;
    let newName = prompt('Give here the new name!');
    newName = newName.trim();
    if(newName != null && newName != '')
        {
            x.innerText = newName;
            updateOnBoard(idForboard, newName);
        }
}


// delete the card from board

function deleteOnBoard(id) {
    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", `${commonUrl}cards/${id}?key=${apiKey}&token=${token}`);
    xhr.send();
}
// rename the card on board

function updateOnBoard(id, newName) {
    var xhr = new XMLHttpRequest();
    xhr.open("PUT", `${commonUrl}cards/${id}?name=${newName}&key=${apiKey}&token=${token}`);
    xhr.send();
}

// add the card on board
async function addCardOnboard(listData, cardName) {
    var xhr = new XMLHttpRequest();
    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === this.DONE) {
          let JSONResponse = JSON.parse(this.response);
          addCard(listData, cardName, JSONResponse.id);
        }
      });
    xhr.open("POST", `${commonUrl}cards?name=${cardName}&idList=${listData.id}&keepFromSource=all&key=${apiKey}&token=${token}`);
    xhr.send();
    
}
